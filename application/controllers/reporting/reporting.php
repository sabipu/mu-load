<?php

class Reporting extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->helper('url_helper');

    if(!$this->session->userdata('admin')) {
      redirect('login');
    }
  }

  function index() {
    $this->load->view('templates/header');
    $this->load->view('reporting/reporting');
    $this->load->view('templates/footer');
  }
}


