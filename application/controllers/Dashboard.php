<?php

class Dashboard extends CI_Controller {
  public function __construct() {
    parent::__construct();

    $this->load->helper('url');

    if(!$this->session->userdata('admin')) {
      redirect('login');
    }
     $this->load->model('activities_model');
     $this->load->model('users_model');
     $this->load->model('units_model');
     $this->load->model('allocation_model');
  }

  function index() {
    $this->db->where('role','academicstaff');
    $this->db->or_where('role','casualstaff');
    $this->db->or_where('role','parttimestaff');
    $this->db->or_where('role','staff');
    $this->db->from('users');
    $data['staffs'] = $this->db->count_all_results();

    $this->db->from('units');
    $data['totalUnits'] = $this->db->count_all_results();

    $this->db->from('activities');
    $data['activities'] = $this->db->count_all_results();

    $this->load->view('templates/header');
    $this->load->view('dashboard', $data);
    $this->load->view('templates/footer');
  }
}