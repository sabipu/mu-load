<div class="reporting_columns">
  <div class="column">
    <div class="report-box">
      <h3>Casual budget status</h3>
      <canvas id="oilChart" width="300" height="200"></canvas>
    </div>
  </div>
  <div class="column">
    <div class="report-box box--half">
      <h3>Staffs</h3>
      <div class="units__wrapper">
        <span class="title">Total Staffs</span>
        <span class="value">16</span>
      </div>
      <a href="#" class="button">Export all staffs</a>
    </div>
    <div class="report-box box--half">
      <h3>Units</h3>
      <div class="units__wrapper">
        <span class="title">Total Units</span>
        <span class="value">5</span>
      </div>
      <a href="#" class="button">Export all units</a>
    </div>
  </div>
  <div class="column">
    <div class="report-box box--half">
      <h3>Allocation Summary</h3>
      <div class="units__wrapper">
        <span class="title">Year</span>
        <span class="value">2019</span>
      </div>
      <a href="#" class="button">Export 2019 allocation</a>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/js/chart.js"></script>
<script>
  var oilCanvas = document.getElementById("oilChart");
  Chart.defaults.global.defaultFontFamily = "Montserrat";
  Chart.defaults.global.defaultFontSize = 15;
  var oilData = {
      labels: [
          "Total budget",
          "Spent so far"
      ],
      datasets: [
          {
              data: [145000,30000],
              backgroundColor: [
                  "#a7e0bd",
                  "#e4776e"
              ]
          }]
  };

  var pieChart = new Chart(oilCanvas, {
    type: 'pie',
    data: oilData
  });
</script>