<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Teaching Load Allocation - Murdoch University</title>
  <meta name="description" content="Work Load Allocation Application for Murdoch University build by Team Bright."/>
  <link type="text/css" rel="stylesheet" href="<?= base_url(); ?>assets/css/dashboard.min.css" media="screen" />
</head>
<body>
  <div id="wrapper">
    <header class="dash__header">
      <div class="dash__logo">
        <a href="/">
          <img src="<?= base_url() ?>assets/images/logo.png" alt="MU Load">
        </a>
      </div>
      <!-- <h1>Dashboard</h1> -->
      <div class="header__right">
        <div class="user__wrap">
          <div class="user__profile">
            <?= file_get_contents(base_url() . 'assets/svg/user.svg') ?>
          </div>
          <div class="user__text">Admin</div>
        </div>
        <a href="<?= base_url() ?>logout" class="logout"><?= file_get_contents(base_url() . 'assets/svg/logout.svg') ?> Logout</a>
      </div>
    </header>
    <aside class="sidebar">
      <ul class="aside__nav">
        <li class="<?php if($this->uri->segment(1)==""){echo "active";}?>">
          <a href="/">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/dashboard.svg') ?>
            </span>
            <span class="nav__text">Dashboard</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1)=="allocation"){echo "active";}?>">
          <a href="/allocation">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/allocation.svg') ?>
            </span>
            <span class="nav__text">Create Allocation</span>
          </a>
        </li>
        <li  class="<?php if($this->uri->segment(1)=="manage-staff" || $this->uri->segment(1)=="register"){echo "active";}?>">
          <a href="/manage-staff">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/staff.svg') ?>
            </span>
            <span class="nav__text">Manage staff</span>
          </a>
        </li>
        <li  class="<?php if($this->uri->segment(1)=="unit"){echo "active";}?>">
          <a href="/unit">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/units.svg') ?>
            </span>
            <span class="nav__text">Units</span>
          </a>
        </li>
        <li  class="<?php if($this->uri->segment(1)=="activities"){echo "active";}?>">
          <a href="/activities">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/activity.svg') ?>
            </span>
            <span class="nav__text">Activities</span>
          </a>
        </li>
        <li  class="<?php if($this->uri->segment(1)=="metrics"){echo "active";}?>">
          <a href="/metrics">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/formula.svg') ?>
            </span>
            <span class="nav__text">Metrics</span>
          </a>
        </li>
        <li  class="<?php if($this->uri->segment(1)=="manage"){echo "active";}?>">
          <a href="/reporting">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/report.svg') ?>
            </span>
            <span class="nav__text">Reports</span>
          </a>
        </li>
        <li  class="setting-menu <?php if($this->uri->segment(1)=="settings"){echo "active";}?>">
          <a href="/settings">
            <span class="nav__icon">
              <?= file_get_contents(base_url() . 'assets/svg/setting.svg') ?>
            </span>
            <span class="nav__text">Settings</span>
          </a>
        </li>
      </ul>
    </aside>
    <div class="dash__container">
